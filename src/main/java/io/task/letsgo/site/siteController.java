package io.task.letsgo.site;

import io.task.letsgo.tasks.Task;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class siteController {

    @RequestMapping({"/", "/hello"})
    public String getMainSite(Model model, @RequestParam(value="name", required = false, defaultValue = "") String name) {
        model.addAttribute("task", new Task());
        model.addAttribute("name", name);
        return "hello";
    }
}
