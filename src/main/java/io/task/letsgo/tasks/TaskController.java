package io.task.letsgo.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TaskController {

    @Autowired
    TaskService taskService;

    @RequestMapping("/tasks")
    public List<Task> getAllTasks(){
        return taskService.getAllTasks();
    }

    @RequestMapping("/tasks/{taskId}")
    public Task getTask(@PathVariable String taskId){
        return taskService.getTask(taskId);
    }

    @RequestMapping(method=RequestMethod.POST, value="/tasks")
    public void addTask(@ModelAttribute("form") @Valid Task task, BindingResult result){
        if(result.hasErrors()){
        }
        else{
            taskService.addTask(task);
        }
    }

    @RequestMapping(method=RequestMethod.PUT, value="/tasks")
    public void updateTask(@RequestBody Task task){
        taskService.updateTask(task);
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/tasks/{taskId}")
    public void deleteTask(@PathVariable String taskId){
        taskService.deleteTask(taskId);
    }

}
